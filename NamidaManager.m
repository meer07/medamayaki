//
//  NamidaManager.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/06.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "NamidaManager.h"
#import "HeroManager.h"
#import "BackGroudManager.h"
#import "animation.h"

@implementation NamidaManager

static NamidaManager *namidaManager = nil;

+(NamidaManager *)namidaManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        // 一度だけnewする
        namidaManager = [[NamidaManager alloc] init];
    });
    return namidaManager;
}

// 涙の初期化とviewへの追加、アニメーション処理を投げてる
-(void)namidaInitAdd{
    CGRect rect = CGRectMake([HeroManager heroManager].hero.layer.position.x-20, 0, 40, 40);
    NamidaImage *namida = [[NamidaImage  alloc] initWithFrame:rect];
    [self.vC.view addSubview:namida];
    [animation namidaanime:namida medamachar:[HeroManager heroManager].hero shinkaview:_shinka background:[BackGroudManager backGround].background];
    [self.vC.view bringSubviewToFront:_shinka];
}

@end
