//
//  ShinkaImage.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/06.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "ShinkaImage.h"


@implementation ShinkaImage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.image = [UIImage imageNamed:@"sinka-effect.png"];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
