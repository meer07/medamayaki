//
//  DataManager.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/15.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

/* 
 このクラスは画像や目玉君のレベルなどゲームのデータを管理するクラス
 シングルトンで実装
*/

#import "DataManager.h"

@implementation DataManager

static DataManager *datamager = nil;

+(DataManager *)dataManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
       // 一度だけnewする
        datamager = [[DataManager alloc] init];
    });
    return datamager;
}

-(void)valueSet{
    _medamalevel =
    _mejikaralevel =
    _animecount = 0;
    
    _anime =
    _tapflag = true;
}
@end
