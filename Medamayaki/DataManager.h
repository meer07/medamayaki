//
//  DataManager.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/15.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

// viewcontroller
@property (nonatomic) UIViewController *vC;

// タップ数と目玉君のレベル(進化度合い)を格納
@property (nonatomic) int tapcount, medamalevel, mejikaralevel, animecount;

// スクリーンサイズを格納
@property (nonatomic) CGRect screensize;

// 画像を格納する配列
@property (nonatomic) NSMutableArray *imagearray;

// 背景画像を格納する配列
@property (nonatomic) NSMutableArray *backgroundimagearray;

// ゲージ画像を格納
@property (nonatomic) NSMutableArray *gaugeimagearray;

// 名前を保存
@property (nonatomic) NSArray *contentname;

// アニメーションの許可
@property (nonatomic) bool anime;

// タップの許可
@property (nonatomic) bool tapflag;

+(DataManager *)dataManager;
-(void)valueSet;
@end
