//
//  Evolution.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/17.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Evolution.h"

@interface Evolution : NSObject
+(void)reset;
+(void)mejikara;
+(void)tapcount;
@end
