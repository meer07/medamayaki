//
//  ViewController.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/13.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "animation.h"
#import "DataManager.h"
#import "Evolution.h"

@interface ViewController : UIViewController

@end
