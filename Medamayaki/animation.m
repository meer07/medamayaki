//
//  animation.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/15.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "animation.h"
#import "imageset.h"
#import "DataManager.h"
#import "BackGroudManager.h"
#import "ViewController.h"

@implementation animation

// アニメーションの動きを決めるマネージャー(アニメーションはランダム)
+(void)animationmanager:(UIImageView *)medama{
    int val = arc4random() % 5;
    
    // NSLog(@"%d",val);
    
    float originX = medama.layer.position.x;
    float originY = medama.layer.position.y;
    
    if([DataManager dataManager].anime == true){
        if (val == 0) {
            if(originX < ([DataManager dataManager].screensize.size.width-(medama.frame.size.width/2))){
                [self walkright:medama];
            }
        } else if(val == 1){
            [self jump:medama];
        }else if(val == 2 ){
            if(originX > (0+(medama.frame.size.width/2))){
                [self walkleft:medama];
            }
        }else if(val == 3 ){
            if(originY > (0+(medama.frame.size.height/2))){
                [self walkup:medama];
            }
        }else if(val == 4){
            if(originY < ([DataManager dataManager].screensize.size.height - (medama.frame.size.height/2))){
                [self walkdown:medama];
            }
        }
    }
}

/*
ここからは目玉自体のアニメーション
*/

// 右に歩くアニメ
+(void)walkright:(UIImageView *)medama{
    [UIView animateWithDuration:2.5f animations:^{
        medama.frame = CGRectOffset(medama.frame, 30, 0);
        [self walkanime:medama];
    }];    
}

// 上に歩くアニメ
+(void)walkup:(UIImageView *)medama{
    [UIView animateWithDuration:2.0f animations:^{
        medama.frame = CGRectOffset(medama.frame, 0, -30);
        [self walkanime:medama];
    }];
}

// 下に歩くアニメ
+(void)walkdown:(UIImageView *)medama{
    [UIView animateWithDuration:2.0f animations:^{
        medama.frame = CGRectOffset(medama.frame, 0, 30);
        [self walkanime:medama];
    }];
}

// 左に歩くアニメ
+(void)walkleft:(UIImageView *)medama{
    [UIView animateWithDuration:2.0f animations:^{
        medama.frame = CGRectOffset(medama.frame, -30, 0);
        [self walkanime:medama];
    }];
}


// ジャンプするアニメ
+(void)jump:(UIImageView *)medama{
    int medamalevel = [DataManager dataManager].medamalevel;
    [UIView animateWithDuration:0.5f animations:^{
        medama.image = [DataManager dataManager].imagearray[medamalevel][3];
        medama.frame = CGRectOffset(medama.frame, 0, -30);
    }completion:^(BOOL finished){
        medama.frame = CGRectOffset(medama.frame, 0, 30);
        medama.image = [DataManager dataManager].imagearray[medamalevel][0];
    }];
}

// 歩くときのパラパラアニメ
+(void)walkanime:(UIImageView *)medama{
    int level = [DataManager dataManager].medamalevel;
    NSMutableArray *images = [NSMutableArray array];
    for (int i = 0; i < 2; i++) {
        [images insertObject:[DataManager dataManager].imagearray[level][i+1] atIndex:i];
    }
    
    medama.animationImages = images;
    medama.animationDuration = 0.5;
    medama.animationRepeatCount = 4;
    [medama startAnimating];
}

/*
 涙のアニメーション(目力のレベルによって進化のアニメに移行)
 */

// 涙のアニメーション(涙の表示と一緒に書いたらバグ(T_T) 出来るだけ処理は別々に書きましょう)
+(void)namidaanime:(UIImageView *)namida medamachar:(UIImageView *)medama shinkaview:(UIImageView *)shinka background:(UIImageView *)bgimage{
    int level = [DataManager dataManager].mejikaralevel;
    if (level == 5) {
        [DataManager dataManager].tapflag = false;
    }
    
    NSLog(@"%d", level);
    [UIView animateWithDuration:2.0f animations:^{
        namida.transform = CGAffineTransformMakeTranslation(0,medama.layer.position.y-medama.frame.size.width/2);
    }completion:^(BOOL finished){
        if (level == 5) {
            [self medamaanime:medama shinkaanime:shinka background:bgimage];
        }
        [namida removeFromSuperview];
    }];
}


/*
 ここからは進化するときの演出のためのアニメーション
 (涙おわり)->(点滅)->(暗転)
 */

// 進化直前のアニメ(点滅)
+(void)medamaanime:(UIImageView *)medama shinkaanime:(UIImageView *)shinka background:(UIImageView *)bgimage{
    [DataManager dataManager].anime = false;
    [UIView animateWithDuration:1.0f animations:^{
        medama.alpha = 0;
    } completion:^(BOOL finshed){
        [self medamaanimeafter:medama shinkaanime:shinka background:bgimage];
    }];
}

+(void)medamaanimeafter:(UIImageView *)medama shinkaanime:(UIImageView *)shinka background:(UIImageView *)bgimage{
    [UIView animateWithDuration:1.0f animations:^{
            medama.alpha = 1.0;
    }completion:^(BOOL finished){
        if ([DataManager dataManager].animecount < 2) {
            [DataManager dataManager].animecount++;
            [self medamaanime:medama shinkaanime:shinka background:bgimage];
        }else{
            [DataManager dataManager].animecount = 0;
            [self shinkaanime:shinka medamaanime:medama background:bgimage];
        }
    }];
}

// 進化のときの暗転アニメ(拡大のみ)
+(void)shinkaanime:(UIImageView *)shinka medamaanime:(UIImageView *)medama background:(UIImageView *)bgimage{
    shinka.hidden = NO;
    [UIView animateWithDuration:2.0f animations:^{
        shinka.transform = CGAffineTransformMakeScale(100, 100);
    }completion:^(BOOL finished){
        // 縮小するときのアニメ処理を投げる
        [DataManager dataManager].medamalevel++;
        
        bgimage.image = [DataManager dataManager].backgroundimagearray[0][[DataManager dataManager].medamalevel];
        medama.image = [DataManager dataManager].imagearray[[DataManager dataManager].medamalevel][0];
        [Evolution reset];
        [self shinkaanimeafter:shinka];
    }];

}

// 縮小のみ
+(void)shinkaanimeafter:(UIImageView *)shinka{
    [UIView animateWithDuration:2.0f animations:^{
        shinka.transform = CGAffineTransformMakeScale(0, 0);
    }completion:^(BOOL finished){
        shinka.hidden = YES;
        [DataManager dataManager].anime = true;
        [DataManager dataManager].tapflag = true;
    }];
    
}
@end
