//
//  Evolution.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/17.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "Evolution.h"
#import "DataManager.h"
#import "animation.h"
#import "imageset.h"


@implementation Evolution

+(void)reset{
    if ([DataManager dataManager].mejikaralevel == 5) {
            [DataManager dataManager].tapcount = 0;
            [DataManager dataManager].mejikaralevel = 0;
    }
}

// 目力ゲージの管理(目玉君が進化するごとに目力ゲージが進行しにくくなる)
+(void)mejikara{
    if ([DataManager dataManager].medamalevel == 0) {
        [DataManager dataManager].mejikaralevel = [DataManager dataManager].tapcount;
    }else if([DataManager dataManager].medamalevel == 1){
        // 2回目薬を差すと目力ゲージが少し進む
        if ([DataManager dataManager].tapcount == 2) {
            [DataManager dataManager].mejikaralevel++;
            [DataManager dataManager].tapcount = 0;
        }
    }else{
        if ([DataManager dataManager].tapcount == 3) {
            [DataManager dataManager].mejikaralevel++;
            [DataManager dataManager].tapcount = 0;
        }
    }
}

+(void)tapcount{
    [DataManager dataManager].tapcount++;
}
@end