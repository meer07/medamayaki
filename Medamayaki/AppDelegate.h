//
//  AppDelegate.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/13.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) int row;
@end
