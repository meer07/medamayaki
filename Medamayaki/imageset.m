//
//  imageset.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/20.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "imageset.h"
#import "DataManager.h"

@implementation imageset

// 画像を配列に格納(viewdidloadで実行)
+(void)imagesset{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:5];
    [array addObject:@[[UIImage imageNamed:@"syoki-main.png"],[UIImage imageNamed:@"syoki-left.png"], [UIImage imageNamed:@"syoki-right.png"],[UIImage imageNamed:@"syoki-jump.png"],[UIImage imageNamed:@"syoki-toji.png"]]];
    [array addObject:@[[UIImage imageNamed:@"ameba-main.png"],[UIImage imageNamed:@"ameba-left.png"], [UIImage imageNamed:@"ameba-right.png"],[UIImage imageNamed:@"ameba-jump.png"],[UIImage imageNamed:@"ameba-toji.png"]]];
    [array addObject:@[[UIImage imageNamed:@"tanpopo-main.png"],[UIImage imageNamed:@"tanpopo-left.png"], [UIImage imageNamed:@"tanpopo-right.png"],[UIImage imageNamed:@"tanpopo-jump.png"],[UIImage imageNamed:@"tanpopo-toji.png"]]];
    [array addObject:@[[UIImage imageNamed:@"ninjya-main.png"],[UIImage imageNamed:@"ninjya-left.png"], [UIImage imageNamed:@"ninjya-right.png"],[UIImage imageNamed:@"ninjya-jump.png"],[UIImage imageNamed:@"ninjya-toji.png"]]];
    [array addObject:@[[UIImage imageNamed:@"daibutsu-main.png"],[UIImage imageNamed:@"daibutsu-left.png"], [UIImage imageNamed:@"daibutsu-right.png"],[UIImage imageNamed:@"daibutsu-jump.png"],[UIImage imageNamed:@"daibutsu-toji.png"]]];
    [array addObject:@[[UIImage imageNamed:@"yama-main.png"],[UIImage imageNamed:@"yama-left.png"], [UIImage imageNamed:@"yama-right.png"],[UIImage imageNamed:@"yama-jump.png"],[UIImage imageNamed:@"yama-toji.png"]]];
    
    [DataManager dataManager].imagearray = array;
}

// 画像を配列に格納(viewdidloadで実行)
+(void)backgroundimagesset{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:7];
    [array addObject:@[[NSNull null],
        [UIImage imageNamed:@"background-ameba.jpg"],
        [UIImage imageNamed:@"background-tanpopo.jpg"],
        [UIImage imageNamed:@"background-ninja2.jpg"],
        [UIImage imageNamed:@"background-daibutsu.jpg"],
        [UIImage imageNamed:@"background-fujisan.jpg"],
        [UIImage imageNamed:@"background-earth.jpg"],]];
    [DataManager dataManager].backgroundimagearray = array;
}

+(void)contentnameset{
    [DataManager dataManager].contentname = @[@"目玉",
                                              @"アメーバ",
                                              @"たんぽぽ",
                                              @"大仏",
                                              @"忍者",
                                              @"富士山",
                                              @"地球"];
}

+(void)gaugeImageSet{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:5];
    [array addObject:@[[UIImage imageNamed:@"tozitame.png"],[UIImage imageNamed:@"nidankai.png"],[UIImage imageNamed:@"hanme.png"],[UIImage imageNamed:@"hiraitame.png"],[UIImage imageNamed:@"akame.png"]]];
    [DataManager dataManager].gaugeimagearray = array;
}

+(void)ImageInit{
    [self imagesset];
    [self backgroundimagesset];
    [self gaugeImageSet];
    [self contentnameset];
}
@end
