//
//  main.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/13.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
