//
//  ViewController.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/01/13.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "ViewController.h"
#import "HeroManager.h"
#import "MegusuriManager.h"
#import "NamidaManager.h"
#import "ShinkaManager.h"
#import "GaugeManager.h"
#import "BackGroudManager.h"
#import "imageset.h"

@interface ViewController (){
    int countdown;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    [[MegusuriManager megusuriManager] megusuriFlagInit];
    countdown = 5;
    [self viewSet];
    [[DataManager dataManager] valueSet];
    [imageset ImageInit];
    [self ScreenSize];
    [self viewAndValueInit];
    [self TimerActtion];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewAndValueInit{
    [[BackGroudManager backGround] initImage];
    [[MegusuriManager megusuriManager] megusuriFlagInit];
    [[HeroManager heroManager] HeroInitAddView];
    [[ShinkaManager shinkaManager] ShinkaInitAdd];
    [[GaugeManager gaugeManager] gaugeInitAdd];

}

-(void)viewSet{
    [GaugeManager gaugeManager].vC =
    [ShinkaManager shinkaManager].vC =
    [NamidaManager namidaManager].vC =
    [HeroManager heroManager].vC =
    [MegusuriManager megusuriManager].vC =
    [BackGroudManager backGround].vC =
    self;
}

// スクリーンサイズを取得
-(void)ScreenSize{
   [DataManager dataManager].screensize = [[UIScreen mainScreen] bounds];
}

// タイマーアクションでボタンの生成
-(void)TimerActtion{
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(Action:) userInfo:nil repeats:YES];
}

-(void)Action:(NSTimer *)timer{
    countdown--;
    
    // 6秒後アニメーションを実行
    if (countdown <= 0) {
        [self Anime];
        countdown = 5;
    }
    
    // 1秒毎にボタンを生成する判定と処理
    [[MegusuriManager megusuriManager] megusuriInitAdd];
}

// 目玉君のアニメーションの処理をanimationに投げる
-(void)Anime{
    [animation animationmanager:[HeroManager heroManager].hero];
}

-(IBAction)zukanButtontapped:(id)sender{
    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backbutton;
    [self performSegueWithIdentifier:@"ToZukan" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
