//
//  ShinkaManager.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/06.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "ShinkaManager.h"
#import "ShinkaImage.h"
#import "NamidaManager.h"
#import "DataManager.h"

@implementation ShinkaManager

static ShinkaManager *shinkaManager = nil;

+(ShinkaManager *)shinkaManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        // 一度だけnewする
        shinkaManager = [[ShinkaManager alloc] init];
    });
    return shinkaManager;
}

// 進化エフェクトの初期化とviewへの追加
-(void)ShinkaInitAdd{
    CGRect scsize = [DataManager dataManager].screensize;
    ShinkaImage *shinka = [ShinkaImage new];
    CGRect rect = CGRectMake(scsize.size.width/2, scsize.size.height/2, 60, 40);
    shinka.frame = rect;
    [self.vC.view addSubview:shinka];
    [NamidaManager namidaManager].shinka = shinka;
    shinka.hidden = YES;
}

@end
