//
//  HeroManager.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/03.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HeroManager : NSObject{
   
}
@property (nonatomic) UIImageView *hero;
@property (nonatomic) UIViewController *vC;
+(HeroManager *)heroManager;
-(void)HeroInitAddView;
@end
