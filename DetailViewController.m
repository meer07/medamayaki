//
//  DetailViewController.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/03/18.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "DetailViewController.h"
#import "DataManager.h"

@interface DetailViewController ()

@end

@implementation DetailViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _imageView.image = [DataManager dataManager].imagearray[0][_row];
}

@end
