//
//  GaugeManager.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/06.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GaugeImage.h"
@interface GaugeManager : NSObject{
    GaugeImage *gauge;
}

@property UIViewController *vC;
+(GaugeManager *)gaugeManager;
-(void)gaugeInitAdd;
-(void)gaugeChangeImage;
@end
