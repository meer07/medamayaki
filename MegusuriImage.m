//
//  MegusuriImage.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/06.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "MegusuriImage.h"
#import "DataManager.h"


@implementation MegusuriImage

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect screct = [DataManager dataManager].screensize;
        srand(time(nil));
        int x = 50 + rand()%((int)screct.size.width-100);
        int y = rand()%((int)screct.size.height-50);
        UIImage *img = [UIImage imageNamed:@"megusuri.png"];
        CGRect rect = CGRectMake(x, y, 70, 70);
        self.frame = rect;
        [self setBackgroundImage:img forState:UIControlStateNormal];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
