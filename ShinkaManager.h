//
//  ShinkaManager.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/06.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShinkaManager : NSObject
@property UIViewController *vC;
+(ShinkaManager *)shinkaManager;
-(void)ShinkaInitAdd;
@end
