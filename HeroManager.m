//
//  HeroManager.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/03.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "HeroManager.h"
#import "HeroImageView.h"
#import "DataManager.h"
#import "NamidaManager.h"

@implementation HeroManager

static HeroManager *heroManager = nil;

+(HeroManager *)heroManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        // 一度だけnewする
        heroManager = [[HeroManager alloc] init];
    });
    return heroManager;
}

// 目玉君の初期化とviewへの追加
-(void)HeroInitAddView{
    HeroImageView *hero = [HeroImageView new];
    hero.image = [DataManager dataManager].imagearray[[DataManager dataManager].medamalevel][0];
    float size = 150;
    CGRect screenSize = [DataManager dataManager].screensize;
    hero.frame = CGRectMake(0, 0, size, size);
    hero.center = CGPointMake(screenSize.size.width/2, screenSize.size.height/2);
    [self.vC.view addSubview:hero];
    _hero = hero;
}

@end
