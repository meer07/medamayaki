//
//  MegusuriManager.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/03.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "MegusuriManager.h"
#import "Evolution.h"
#import "animation.h"
#import "GaugeManager.h"
#import "NamidaManager.h"
#import "BackGroudManager.h"
#import "DataManager.h"

@implementation MegusuriManager

static MegusuriManager *megusuriManager = nil;

+(MegusuriManager *)megusuriManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        // 一度だけnewする
        megusuriManager = [[MegusuriManager alloc] init];
    });
    return megusuriManager;
}

-(void)megusuriFlagInit{
    buttonhidden = true;
}

// 目薬ボタンの初期化とviewへの追加
-(void)megusuriInitAdd{
    if (buttonhidden == true) {
        buttonhidden = false;
        megusuri = [MegusuriImage new];
        [megusuri addTarget:self action:@selector(megusuriEvent:) forControlEvents:UIControlEventTouchDown];
        [self.vC.view addSubview:megusuri];
        [self.vC.view sendSubviewToBack:megusuri];
        [self.vC.view sendSubviewToBack:[BackGroudManager backGround].background];
        //[self.vC.view sendSubviewToBack:];
    }
}

// ボタンイベント
-(void)megusuriEvent:(UIButton *)button{
    if([DataManager dataManager].tapflag){
        [megusuri removeFromSuperview];
        buttonhidden = true;
        [Evolution tapcount];
        [Evolution mejikara];
        [[GaugeManager gaugeManager] gaugeChangeImage];
        [[NamidaManager namidaManager] namidaInitAdd];
    }
}

@end
