//
//  BackGroudManager.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/13.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "BackGroudManager.h"
#import "BackgroundImage.h"
#import "DataManager.h"

@implementation BackGroudManager

static BackGroudManager *backGroundManager_;

+(BackGroudManager *)backGround{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        backGroundManager_ = [[BackGroudManager alloc] init];
    });
    return backGroundManager_;
}

-(void)initImage{
    BackgroundImage *background = [BackgroundImage new];
    CGRect screens = [DataManager dataManager].screensize;
    background.frame = CGRectMake(0, 0, screens.size.width, screens.size.height);
    [self.vC.view addSubview:background];
    [self.vC.view bringSubviewToFront:background];
    _background = background;
}

@end
