//
//  MegusuriManager.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/03.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MegusuriImage.h"

@interface MegusuriManager : NSObject{
    MegusuriImage *megusuri;
    bool buttonhidden;
}

@property (nonatomic) UIViewController *vC;

+(MegusuriManager *)megusuriManager;
-(void)megusuriInitAdd;
-(void)megusuriFlagInit;
@end
