//
//  ZukanViewCell.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/03/18.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZukanViewCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UILabel *label;

@end
