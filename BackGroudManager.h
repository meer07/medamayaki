//
//  BackGroudManager.h
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/13.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackGroudManager : NSObject{
}

@property (nonatomic) UIViewController *vC;
@property (nonatomic) UIImageView *background;
+(BackGroudManager *)backGround;
-(void)initImage;
@end
