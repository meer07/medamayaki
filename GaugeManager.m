//
//  GaugeManager.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/02/06.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "GaugeManager.h"
#import "DataManager.h"

@implementation GaugeManager

static GaugeManager *gaugeManager = nil;

+(GaugeManager *)gaugeManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        // 一度だけnewする
        gaugeManager = [[GaugeManager alloc] init];
    });
    return gaugeManager;
}

// 目力ゲージの初期化とviewへの追加
-(void)gaugeInitAdd{
    CGRect rect = CGRectMake(5, 25, 60, 40);
    gauge = [[GaugeImage alloc] initWithFrame:rect];
    gauge.image = [DataManager dataManager].gaugeimagearray[0][0];
    [self.vC.view addSubview:gauge];
}


// ゲージの画像切り替え
-(void)gaugeChangeImage{
    if ([DataManager dataManager].mejikaralevel < 5) {
        gauge.image = [DataManager dataManager].gaugeimagearray[0][[DataManager dataManager].mejikaralevel];
    }else{
        gauge.image = [DataManager dataManager].gaugeimagearray[0][0];
    }

}
@end
