//
//  ZukanViewController.m
//  Medamayaki
//
//  Created by 海下直哉 on 2014/03/18.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import "ZukanViewController.h"
#import "DetailViewController.h"
#import "DataManager.h"
#import "AppDelegate.h"

@interface ZukanViewController ()

@end

@implementation ZukanViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell" ];
    cell.textLabel.text = [DataManager dataManager].contentname[indexPath.row];
    
    if (indexPath.row <= [DataManager dataManager].medamalevel) {
        cell.imageView.image = [DataManager dataManager].imagearray[indexPath.row][0];
    }else{
        cell.imageView.image = [UIImage imageNamed:@"question.png"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AppDelegate *aD = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    aD.row = indexPath.row;
    [self performSegueWithIdentifier:@"Detail" sender:self];
}
@end
